package com.jdbc;

import java.sql.*;

public class DBController
{
    private DBConnection connection;
    private int countOfProducts = 0;
    Statement statement;
    Main gui;

    public DBController(int countOfProducts, DBConnection connection) throws SQLException
    {
        this.connection = connection;
        try
        {
            String cleanTable = "truncate table business.products";
            this.statement = connection.getConnection().createStatement();
            try
            {
                statement.executeUpdate(cleanTable);
            }
            catch (Exception e)
            {
                statement.executeUpdate(createTable("business.products"));
            }

            for (int i = 0; i < countOfProducts; ++i)
            {
                add("товар" + i, (int) (Math.random() * 10000));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (statement != null)
            {
                statement.close();
            }
        }
    }

    private String createTable(String tableName)
    {
        String createTable = "create table " + tableName +
                "(id int(6) unsigned auto_increment primary key, " +
                "product_id int(6) not null, " +
                "product_name varchar(45) not null, " +
                "price double(20, 2) unsigned not null)";
        return createTable;
    }

    public void add(String name, double price/*, int prod_id*/) throws SQLException
    {
        String trySelectExisting = "select product_name from business.products where product_name=?";
        try
        {
            PreparedStatement prepTrySelect = connection.getConnection().prepareStatement(trySelectExisting);
            prepTrySelect.setString(1, name);;
            ResultSet resultSet = prepTrySelect.executeQuery();
            if (resultSet.next())
            {
                gui.showErrorWindow("There is product with this name");
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String insert = "insert into business.products (product_id, product_name, price) values (?, ?, ?)";
        try
        {
            /*PreparedStatement prepInsert = connection.getConnection().prepareStatement(insert);
            prepInsert.setInt(1, prod_id);
            prepInsert.setString(2, name);
            prepInsert.setDouble(3, price);
            prepInsert.execute();*/

            ++countOfProducts;
            PreparedStatement prepInsert = connection.getConnection().prepareStatement(insert);
            prepInsert.setInt(1, countOfProducts);
            prepInsert.setString(2, name);
            prepInsert.setDouble(3, price);
            prepInsert.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void delete(String name) throws SQLException
    {
        String delete = "delete from business.products where product_name=?";
        try
        {
            PreparedStatement prepDelete = connection.getConnection().prepareStatement(delete);
            prepDelete.setString(1, name);
            prepDelete.executeUpdate();
            --countOfProducts;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String[] showAll() throws SQLException
    {
        if (countOfProducts <= 0)
        {
            return new String[]{"There are no products in table"};
        }
        String[] results = new String[countOfProducts];
        String show = "select * from business.products";
        try
        {
            PreparedStatement prepShow = connection.getConnection().prepareStatement(show);
            ResultSet resultSet = prepShow.executeQuery();
            results = output(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return results;
    }

    public String[] getNames()
    {
        String[] results = new String[countOfProducts];
        int i = 0;
        String getNames = "select product_name from business.products";
        try
        {
            PreparedStatement prepGet = connection.getConnection().prepareStatement(getNames);
            ResultSet resultSet = prepGet.executeQuery();
            while (resultSet.next())
            {
                results[i] = resultSet.getString("product_name");
                ++i;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return results;
    }

    public String getPrice(String name) throws SQLException
    {
        double price = 0;
        String getPrice = "select price from business.products where product_name=?";
        try
        {
            PreparedStatement prepGetPrice = connection.getConnection().prepareStatement(getPrice);
            prepGetPrice.setString(1, name);
            ResultSet resultSet = prepGetPrice.executeQuery();
            if (resultSet.next())
            {
                price = resultSet.getDouble("price");
            }
            else
            {
                gui.showErrorWindow("No product with this name");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return Double.toString(price);
    }

    public void changePrice(String name, double newPrice) throws SQLException
    {
        String changePrice = "update business.products set price=? where product_name=?";
        try
        {
            PreparedStatement prepChangePrice = connection.getConnection().prepareStatement(changePrice);
            prepChangePrice.setDouble(1, newPrice);
            prepChangePrice.setString(2, name);
            prepChangePrice.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String[] showInterval(double from, double to) throws SQLException
    {
        String[] results = new String[countOfProducts];
        String showInterval = "select * from business.products where price between ? AND ?";
        try
        {
            PreparedStatement prepShowInterval = connection.getConnection().prepareStatement(showInterval);
            prepShowInterval.setDouble(1, from);
            prepShowInterval.setDouble(2, to);
            ResultSet resultSet = prepShowInterval.executeQuery();
            results = output(resultSet);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return results;
    }

    private String[] output(ResultSet resultSet) throws SQLException
    {
        String[] results = new String[countOfProducts];
        int i = 0;
        while (resultSet.next())
        {
             results[i] = ("\t    " + resultSet.getInt("id") +
                    "\t\t\t\t  " + resultSet.getInt("product_id") +
                    "\t\t\t\t\t      " + resultSet.getString("product_name") +
                    "\t\t\t\t\t\t   " + resultSet.getDouble("price"));
             ++i;
        }

        return results;
    }

    public int getCount()
    {
        return countOfProducts;
    }

    public void setGUI(Main gui)
    {
        this.gui = gui;
    }
}
