package com.jdbc;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.sql.SQLException;

public class MainWindowController
{
    Main gui;
    DBController dbController;
    String nameOfSelectedProduct;
    String nameNewProduct;
    double priceNewProduct;
    double priceFrom;
    double priceTo;

    @FXML
    private TextArea outputProducts;
    @FXML
    private TextArea console;
    @FXML
    private ComboBox<String> namesBoxChangePrice = new ComboBox<String>();
    @FXML
    private ComboBox<String> namesBoxShowPrice = new ComboBox<String>();
    @FXML
    private ComboBox<String> namesBoxDelete = new ComboBox<String>();
    @FXML
    private TextField newPrice;
    @FXML
    private TextField addPrice;
    @FXML
    private TextField addName;
    @FXML
    private TextField filterFrom;
    @FXML
    private TextField filterTo;
    @FXML
    private SplitPane newPriceWindow;
    @FXML
    private Pane deletePain;
    @FXML
    private Pane addPain;
    @FXML
    private Pane filterPain;
    @FXML
    private Button showAllButton;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button showPriceButton;
    @FXML
    private Button changePriceButton;
    @FXML
    private Button filterButton;

    private void disableButtons()
    {
        showAllButton.setDisable(true);
        addButton.setDisable(true);
        deleteButton.setDisable(true);
        showPriceButton.setDisable(true);
        changePriceButton.setDisable(true);
        filterButton.setDisable(true);
    }

    private void activeButtons()
    {
        showAllButton.setDisable(false);
        addButton.setDisable(false);
        deleteButton.setDisable(false);
        showPriceButton.setDisable(false);
        changePriceButton.setDisable(false);
        filterButton.setDisable(false);
    }

    @FXML
    private void showAll() throws SQLException
    {
        String[] result = dbController.showAll();
        StringBuilder newString = new StringBuilder();
        outputProducts.setPrefColumnCount(result.length);
        for (int i = 0; i < result.length; ++i)
        {
            newString.append("\n").append(result[i]);
        }
        outputProducts.setText(newString.toString());
    }

    @FXML
    private void approveAdd() throws SQLException
    {
        activeButtons();
        addPain.setVisible(false);
        try
        {
            nameNewProduct = addName.getText();
            priceNewProduct = Integer.parseInt(addPrice.getText());
            if (priceNewProduct < 0)
            {
                gui.showErrorWindow("Please enter positive number");
                addPrice.clear();
                addName.clear();
            }
            else
            {
                dbController.add(nameNewProduct, priceNewProduct);
            }
        }
        catch (Exception e)
        {
            gui.showErrorWindow("Please enter correct number");
        }
        addPrice.clear();
        addName.clear();
    }

    @FXML
    private void add()
    {
        try
        {
            outputProducts.clear();
            disableButtons();
            addPain.setVisible(true);
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    @FXML
    private void approveDelete() throws SQLException
    {
        activeButtons();
        deletePain.setVisible(false);
        try
        {
            dbController.delete(namesBoxDelete.getValue());
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    @FXML
    private void delete()
    {
        disableButtons();
        if (dbController.getCount() <= 0)
        {
            outputProducts.setText("There are no product in table");
            return;
        }
        try
        {
            outputProducts.clear();
            namesBoxDelete.getItems().clear();
            String[] result = dbController.getNames();
            deletePain.setVisible(true);
            for (String s : result) {
                namesBoxDelete.getItems().add(s);
            }
            namesBoxDelete.setOnAction(actionEvent -> nameOfSelectedProduct = namesBoxDelete.getValue());
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    @FXML
    private void showPrice()
    {
        disableButtons();
        if (dbController.getCount() <= 0)
        {
            outputProducts.setText("There are no product in table");
            return;
        }
        try
        {
            outputProducts.clear();
            namesBoxShowPrice.getItems().clear();
            String[] result = dbController.getNames();
            namesBoxShowPrice.setVisible(true);
            for (int i = 0; i < result.length; ++i)
            {
                namesBoxShowPrice.getItems().add(result[i]);
            }
            namesBoxShowPrice.setOnAction(actionEvent -> {
                try
                {
                    showPriceAfterSelect(namesBoxShowPrice.getValue());
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
            });
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    private void showPriceAfterSelect(String name) throws SQLException
    {
        activeButtons();
        nameOfSelectedProduct = name;
        namesBoxShowPrice.setVisible(false);
        String resultPrice = dbController.getPrice(nameOfSelectedProduct);
        outputProducts.setText("Цена продукта " + nameOfSelectedProduct + ":\t\t" + resultPrice);
    }

    @FXML
    private void approveNewPrice() throws SQLException
    {
        activeButtons();
        newPriceWindow.setVisible(false);
        double price = 0;
        try
        {
            price = Double.parseDouble(newPrice.getText());
            if (price < 0)
            {
                gui.showErrorWindow("Select positive price");
            }
            dbController.changePrice(nameOfSelectedProduct, price);
            newPrice.clear();
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    @FXML
    private void changePrice()
    {
        disableButtons();
        if (dbController.getCount() <= 0)
        {
            outputProducts.setText("There are no product in table");
            return;
        }
        try
        {
            outputProducts.clear();
            namesBoxChangePrice.getItems().clear();
            String[] result = dbController.getNames();
            newPriceWindow.setVisible(true);
            for (int i = 0; i < result.length; ++i)
            {
                namesBoxChangePrice.getItems().add(result[i]);
            }
            namesBoxChangePrice.setOnAction(actionEvent -> nameOfSelectedProduct = namesBoxChangePrice.getValue());
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    @FXML
    private void approveFilter()
    {
        try
        {
            activeButtons();
            filterPain.setVisible(false);
            priceFrom = Double.parseDouble(filterFrom.getText());
            priceTo = Double.parseDouble(filterTo.getText());
            if (priceFrom < 0 || priceTo < 0)
            {
                gui.showErrorWindow("Please enter positive number");
                filterFrom.clear();
                filterTo.clear();
            }
            else if (priceFrom > priceTo)
            {
                gui.showErrorWindow("Left border more than right border");
                filterFrom.clear();
                filterTo.clear();
            }
            else
            {
                String[] result = dbController.showInterval(priceFrom, priceTo);
                StringBuilder newString = new StringBuilder();
                outputProducts.setPrefColumnCount(result.length);
                for (int i = 0; i < result.length; ++i)
                {
                    if (result[i] != null)
                        newString.append("\n").append(result[i]);
                }
                outputProducts.setText(newString.toString());
            }
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
        filterFrom.clear();
        filterTo.clear();
    }

    @FXML
    private void filter()
    {
        if (dbController.getCount() <= 0)
        {
            outputProducts.setText("There are no product in table");
            return;
        }
        try
        {
            outputProducts.clear();
            disableButtons();
            filterPain.setVisible(true);
        }
        catch (Exception e)
        {
            gui.showErrorWindow(e.getMessage());
        }
    }

    public void setGUI(Main gui)
    {
        this.gui = gui;
    }

    public void setDbController(DBController controller)
    {
        this.dbController = controller;
    }
}
