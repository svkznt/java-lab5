package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection
{
    private final String USERNAME = "root";
    private final String PASSWORD = "roott";
    private final String URL = "jdbc:mysql://localhost:3306/mysql?useSSL=false";
    private Connection connection;
    private DBConnection instance;

    DBConnection() throws SQLException
    {
        this.connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public DBConnection getInstance() throws SQLException
    {
        if (instance == null)
        {
            instance = new DBConnection();
        }
        return instance;
    }

    public Connection getConnection()
    {
        return connection;
    }
}
