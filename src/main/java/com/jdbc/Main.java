package com.jdbc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class Main extends Application
{
    Stage primaryStage;
    static DBConnection dbConnection = null;
    static DBController dbController;

    public static void main(String[] args) throws SQLException
    {
        try
        {
            dbConnection = new DBConnection().getInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.primaryStage = primaryStage;
        primaryStage.setResizable(false);
        showLoginWindow();
        primaryStage.show();
    }

    public void showLoginWindow()
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/enterWindow.fxml"));
            primaryStage.setTitle("Enter number");
            primaryStage.setScene(new Scene(loader.load()));
            EnterController enterController = loader.getController();
            enterController.setGUI(this);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void showMainWindow(int number)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/mainWindow.fxml"));
            primaryStage.setTitle("Main menu");
            primaryStage.setScene(new Scene(loader.load()));
            MainWindowController controller = loader.getController();
            controller.setGUI(this);
            dbController  = new DBController(number, dbConnection);
            dbController.setGUI(this);
            controller.setDbController(dbController);
        }
        catch (IOException | SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void showErrorWindow(String message)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(primaryStage);
        alert.setTitle("Error");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
