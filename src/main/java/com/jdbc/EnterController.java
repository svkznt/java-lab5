package com.jdbc;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class EnterController
{
    Main gui;

    @FXML
    private TextField number;

    @FXML
    private void buttonClicked()
    {
        try
        {
            int n = Integer.parseInt(number.getText());
            if (n < 0)
            {
                gui.showErrorWindow("Please enter positive number");
                number.clear();
            }
            else
            {
                gui.showMainWindow(n);
            }
        }
        catch (NumberFormatException e)
        {
            gui.showErrorWindow("Please enter correct number");
        }
    }

    public void setGUI(Main gui)
    {
        this.gui = gui;
    }
}
